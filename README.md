# FileLoader
FileLoader allows to access in local files and remote files in uniform way. 

## Feaures:
 - Remote files can be accessible thought HTTP and FTP protocols.
 - The files can be cached locally.

## Usage
For usage visit [Download Files with Fileloader tutorial](http://www.napuzba.com/story/download-files-with-fileloader/).